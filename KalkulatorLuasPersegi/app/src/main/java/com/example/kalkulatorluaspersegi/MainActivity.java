package com.example.kalkulatorluaspersegi;

import static java.sql.Types.NULL;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    EditText inputSisi;
    Button btnHitung, btnRefresh;
    TextView ViewLuas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        inputSisi=(EditText) findViewById(R.id.InputSisi);
        btnHitung=(Button) findViewById(R.id.btn);
        btnRefresh=(Button) findViewById(R.id.btnRefresh);
        ViewLuas=(TextView) findViewById(R.id.HasilLuas);

        btnHitung.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                double sisi,hasil;
                sisi=Double.valueOf(inputSisi.getText().toString().trim());
                hasil=sisi*sisi;
                String hasil1=String.valueOf(hasil);
                ViewLuas.setText(hasil1);
            }
        });

        btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                double hasil;
                hasil=0.0;
                String LuasRefresh=String.valueOf(hasil);
                inputSisi.setText("");
                ViewLuas.setText(LuasRefresh);

            }
        });
    }
}