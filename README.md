# PPK-Praktikum 10 : Event Handling

## Identitas

```
Nama : Gilang W Prasetyo
NIM  : 222011750
Kelas: 3SI3

```

## Deskripsi

Terdapat dua cara untuk menangani event pada pemrograman android, yang pertama adalah 
dengan mendaftarkan event pada xml activity. Yang kedua adalah dengan menambahkannya 
secara dinamis pada file java activity. 

## Kegiatan Praktikum

### 1. Hello World
![cd_catalog.xml](https://gitlab.com/matkulpepeka/ppk-praktikum-10/-/blob/master/Screenshot/SS1.jpg)
### 2. Button Clicked
![cd_catalog.xml](https://gitlab.com/matkulpepeka/ppk-praktikum-10/-/blob/master/Screenshot/SS2.jpg)
### 3. Button 2 Clicked
![cd_catalog.xml](https://gitlab.com/matkulpepeka/ppk-praktikum-10/-/blob/master/Screenshot/SS3.jpg)

## Penugasan Praktikum : Kalkulator Luas Persegi

### 1. Tampilan Awal Kalkulator
![cd_catalog.xml](https://gitlab.com/matkulpepeka/ppk-praktikum-10/-/blob/master/Screenshot/persegi1.jpg)
### 3. Operasi Kalkulator
![cd_catalog.xml](https://gitlab.com/matkulpepeka/ppk-praktikum-10/-/blob/master/Screenshot/persegi2.jpg)
